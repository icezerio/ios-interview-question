//
//  ConnectionController.swift
//  Question
//
//  Created by Icezerio on 6/1/2564 BE.
//

import Alamofire
import SwiftyJSON
import SystemConfiguration

class ConnectionController {
    
    static let share = ConnectionController()
    
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        return ret
    }
    
    public static var accessToken: String {
        get {
            guard let token = UserDefaults().object(forKey: "ACCESS_TOKEN") as? String else { return "" }
            return token
        }
        set(token) {  UserDefaults().set(token, forKey: "ACCESS_TOKEN") }
    }
    
    func makeRequest(_ path: String, method: HTTPMethod = .get, parameters: Parameters = [:], headers: HTTPHeaders = [:], auth: Bool = false,haveEncode: Bool = true,query: Bool = false,onCompletion: @escaping (JSON?) -> Void, onError: @escaping (String, String?) -> Void) {
        if !ConnectionController.isConnectedToNetwork() {
//            onError(Properties.Message.MESSAGE_ERROR_INTERNET, nil)
            return
        }
        
        var encoding: ParameterEncoding = URLEncoding.default
        if haveEncode {
            if (method == .put || method == .post) || method == .patch {
                encoding = JSONEncoding.default
            }
            if query {
                encoding = URLEncoding.queryString
            }
        }
        
        var copyHeaders = headers
        if auth {
            copyHeaders["authorization"] = "Bearer \(ConnectionController.accessToken)"
        }
        if (copyHeaders["lang"] == nil){
            copyHeaders["lang"] = "th"
        }
        let url = path.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        manager.request(url, method: method, parameters: parameters, encoding: encoding, headers: copyHeaders).responseJSON { response in
            switch response.result {
            case .success:
                if ((response.response?.statusCode)! >= 200){
                    let json = JSON(response.result.value!)
                    print("JSON : \(json)")
                    if response.response?.statusCode == 200 {
                        onCompletion(json)
                    }
                }
            case .failure:
                print("Fail...")
            }
        }
    }
}
