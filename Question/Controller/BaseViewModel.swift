//
//  BaseViewModel.swift
//  Question
//
//  Created by Icezerio on 6/1/2564 BE.
//

import UIKit

public class BaseViewModel {
    
    func goalVC() -> GoalViewController {
        return (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Goal") as? GoalViewController)!
    }
}
