//
//  TabbarController.swift
//  Question
//
//  Created by Icezerio on 7/1/2564 BE.
//

import UIKit

class TabbarController: UITabBarController, UITabBarControllerDelegate {
    override func viewDidLoad() {
        self.delegate = self
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if viewController is ProfileViewController {
            SocketHelper.Events.search.emit()
        }
    }
}
