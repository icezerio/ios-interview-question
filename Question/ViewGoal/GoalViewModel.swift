//
//  GoalViewModel.swift
//  Question
//
//  Created by Icezerio on 6/1/2564 BE.
//

import Alamofire
import RxSwift

class GoalViewModel: BaseViewModel {
    var menuList = [MenuObject]()
    let rx_menuList: BehaviorSubject<[MenuObject]?> = BehaviorSubject(value: nil)
    
    func getMenu() {
        menuList.append(MenuObject(image: "travel", name: "Travel"))
        menuList.append(MenuObject(image: "education", name: "Education"))
        menuList.append(MenuObject(image: "invest", name: "Invest"))
        menuList.append(MenuObject(image: "clothing", name: "Clothing"))
        menuList.append(MenuObject(image: "education", name: "Education"))
        self.rx_menuList.onNext(menuList)
    }
}
