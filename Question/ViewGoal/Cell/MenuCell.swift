//
//  MenuCell.swift
//  Question
//
//  Created by Icezerio on 6/1/2564 BE.
//

import UIKit

class MenuCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var isCurve = false
    
    override func awakeFromNib() {
        bgView.setBorderAchievement(0)
    }
    
    func apply(item: MenuObject) {
        if isCurve {
            bgView.setBorderMenu(6)
            bgView.dropShadow()
        } else {
            bgView.setBorderAchievement(0)
        }
        imageView.image = UIImage(named: item.image)
        titleLabel.text = item.name
    }
    
}
