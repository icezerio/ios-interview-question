//
//  GoalViewController.swift
//  Question
//
//  Created by Icezerio on 5/1/2564 BE.
//

import UIKit
import RxSwift

class GoalViewController: BaseViewController {
    
    @IBOutlet weak var moneyView: UIView!
    @IBOutlet weak var selectAccountView: UIView!
    @IBOutlet weak var selectDateView: UIView!
    @IBOutlet weak var amountView: UIView!
    @IBOutlet weak var menuCollectionView: UICollectionView!
    lazy var vm: GoalViewModel = {
        return GoalViewModel()
    }()
    
    fileprivate let disposeBag = DisposeBag()
    var menu = [MenuObject]()

    @IBAction func backClick(_ sender: UIButton) {
        self.popVC()
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        setupUI()
        vm.getMenu()
        
        vm.rx_menuList
            .filter { $0 != nil }
            .map { $0! }
            .subscribe(onNext: { [unowned self] menu in
                self.menu = menu
                self.menuCollectionView.reloadData()
        }).disposed(by: disposeBag)
    }
    
    func setupUI() {
        amountView.setBorderAchievement(0)
        selectDateView.setBorderAchievement(0)
        selectAccountView.setBorderAchievement(0)
        moneyView.setBorderAchievement(0)
        
    }
}

extension GoalViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menu.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: MenuCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCell", for: indexPath) as! MenuCell
        cell.isCurve = true
        cell.apply(item: menu[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var collectionViewSize = collectionView.frame.size
        collectionViewSize.width = collectionViewSize.width/3.2
        collectionViewSize.height = (collectionViewSize.height*0.4)
        return collectionViewSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    
}
