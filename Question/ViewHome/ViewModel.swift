//
//  ViewModel.swift
//  Question
//
//  Created by Icezerio on 6/1/2564 BE.
//

import Alamofire
import RxSwift

class ViewModel: BaseViewModel {
    var summaryList = [SummaryObject]()
    var bestList = [MenuObject]()
    var suggestList = [MenuObject]()
    let rx_summaryList: BehaviorSubject<[SummaryObject]?> = BehaviorSubject(value: nil)
    let rx_bestList: BehaviorSubject<[MenuObject]?> = BehaviorSubject(value: nil)
    let rx_suggestList: BehaviorSubject<[MenuObject]?> = BehaviorSubject(value: nil)
    
    func getSummary() {
        summaryList.append(SummaryObject(image: "travel", title: "ไปเที่ยวญี่ปุ่น", discount: 16500, price: 20000, status: true, time: "45"))
        summaryList.append(SummaryObject(image: "invest", title: "ซื้อกองทุนรวม", discount: 500, price: 6000, status: false, time: "20"))
        summaryList.append(SummaryObject(image: "travel", title: "ไปทะเล", discount: 4000, price: 10000, status: true, time: "15"))
        summaryList.append(SummaryObject(image: "education", title: "ไปเรียนต่างประเทศ", discount: 100000, price: 1200000, status: false, time: "10"))
        self.rx_summaryList.onNext(summaryList)
    }
    
    func getImageBestOffer() {
        bestList.append(MenuObject(image: "unnamed", name: ""))
        bestList.append(MenuObject(image: "sale", name: ""))
        bestList.append(MenuObject(image: "major", name: ""))
        bestList.append(MenuObject(image: "sale", name: ""))
        bestList.append(MenuObject(image: "unnamed", name: ""))
        self.rx_bestList.onNext(bestList)
    }
    
    func getImageSuggest() {
        suggestList.append(MenuObject(image: "sale", name: ""))
        suggestList.append(MenuObject(image: "unnamed", name: ""))
        suggestList.append(MenuObject(image: "major", name: ""))
        self.rx_suggestList.onNext(suggestList)
    }
}
