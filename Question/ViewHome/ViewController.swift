//
//  ViewController.swift
//  Question
//
//  Created by Icezerio on 5/1/2564 BE.
//

import UIKit
import RxSwift

class ViewController: BaseViewController {
    
    @IBOutlet weak var summaryCollectionView: UICollectionView!
    
    @IBOutlet weak var bestOfferCollectionView: UICollectionView!
    
    @IBOutlet weak var suggestCollectionView: UICollectionView!
    
    @IBOutlet weak var goalNumberLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var newGoalButton: UIButton!
    
    fileprivate let disposeBag = DisposeBag()
    
    var summary = [SummaryObject]()
    var best = [MenuObject]()
    var suggest = [MenuObject]()
    
    lazy var vm: ViewModel = {
        return ViewModel()
    }()
    
    @IBAction func newGoalClick(_ sender: UIButton) {
        self.pushVC(vc: vm.goalVC())
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        vm.getSummary()
        vm.getImageBestOffer()
        vm.getImageSuggest()
        
        vm.rx_summaryList
            .filter { $0 != nil }
            .map { $0! }
            .subscribe(onNext: { [unowned self] summary in
                self.summary = summary
                self.setupData()
        }).disposed(by: disposeBag)
        
        vm.rx_bestList
            .filter { $0 != nil }
            .map { $0! }
            .subscribe(onNext: { [unowned self] best in
                self.best = best
                self.bestOfferCollectionView.reloadData()
        }).disposed(by: disposeBag)
        
        vm.rx_suggestList
            .filter { $0 != nil }
            .map { $0! }
            .subscribe(onNext: { [unowned self] suggest in
                self.suggest = suggest
                self.suggestCollectionView.reloadData()
        }).disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func setupUI() {
        buttonView.roundCorners(6)
        
    }
    
    func setupData() {
        self.goalNumberLabel.text = "\(summary.count) Goals"
        var total = 0
        for item in summary {
            total += item.discount
        }
        self.totalLabel.text = "All Saving \(total.addCommas()) THB"
        self.summaryCollectionView.reloadData()
    }

}

extension ViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == summaryCollectionView {
            return summary.count
        } else if collectionView == bestOfferCollectionView {
            return best.count
        } else {
            return suggest.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == summaryCollectionView {
            let cell: SummaryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SummaryCell", for: indexPath) as! SummaryCell
            cell.setViewConstraint(item: summary[indexPath.row])
            cell.apply(item: summary[indexPath.row])
            return cell
        } else {
            let cell: ImageCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageCell
            if collectionView == bestOfferCollectionView {
                cell.apply(item: best[indexPath.row])
            } else {
                cell.apply(item: suggest[indexPath.row])
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var collectionViewSize = collectionView.frame.size
        if collectionView == summaryCollectionView {
            collectionViewSize.width = collectionViewSize.width/2.4
            collectionViewSize.height = (collectionViewSize.height*0.8)
        } else {
            collectionViewSize.width = collectionViewSize.width/1.5
            collectionViewSize.height = (collectionViewSize.height*1)
        }
        return collectionViewSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}

