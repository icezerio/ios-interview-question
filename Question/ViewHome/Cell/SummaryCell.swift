//
//  SummaryCell.swift
//  Question
//
//  Created by Icezerio on 5/1/2564 BE.
//

import UIKit

class SummaryCell: UICollectionViewCell {
    
    @IBOutlet weak var blackView: UIView!
    @IBOutlet weak var widthConst: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        lineView.roundCorners(6)
    }
    
    func apply(item: SummaryObject) {
        imageView.image = UIImage(named: item.image)
        discountLabel.text = "\(item.discount.addCommas()) THB"
        priceLabel.text = "\(item.price.addCommas()) THB"
        titleLabel.text = item.title
        if item.status {
            statusLabel.text = "Good"
            statusLabel.textColor = UIColor.greenColor
            bgView.setBorderGood(6)
        } else {
            statusLabel.text = "Unhappy"
            statusLabel.textColor = UIColor.redColor
            bgView.setBorderfail(6)
        }
        timeLabel.text = "\(item.time) days left"
    }
    
    func setViewConstraint(item: SummaryObject) {
        let average = (Double(item.discount*100)/Double(item.price))/100
        let newConstraint = widthConst.constraintWithMultiplier(CGFloat(1-average))
        lineView.removeConstraint(widthConst)
        lineView.addConstraint(newConstraint)
        self.layoutIfNeeded()
        widthConst = newConstraint
    }
}

extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem!, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    }
}
