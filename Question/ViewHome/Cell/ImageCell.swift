//
//  ImageCell.swift
//  Question
//
//  Created by Icezerio on 6/1/2564 BE.
//

import UIKit

class ImageCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        
    }
    
    func apply(item: MenuObject) {
        imageView.image = UIImage(named: item.image)
    }
}
