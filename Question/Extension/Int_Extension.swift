//
//  Int_Extension.swift
//  Question
//
//  Created by Icezerio on 6/1/2564 BE.
//

import Foundation

extension Int {
    
    public func addCommas() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        let formattedNumber = numberFormatter.string(from: NSNumber(value:self)) ?? ""
        return formattedNumber
    }
}
