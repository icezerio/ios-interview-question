//
//  UIViewController_Extension.swift
//  Question
//
//  Created by Icezerio on 6/1/2564 BE.
//

import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func pushVC(vc: UIViewController) {
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushNoVC(vc: UIViewController) {
        navigationController?.pushViewController(vc, animated: false)
    }
    
    func popVC() {
        navigationController?.popViewController(animated: true)
    }
    
    func popNoVC() {
        navigationController?.popViewController(animated: false)
    }
    
    func presentVC(vc: UIViewController) {
        present(vc, animated: true, completion: nil)
    }
    
    func dismissVC() {
        dismiss(animated: true, completion: nil)
    }
    
    func hideNav() {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func showNav() {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func showTabbar() {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func setupPresent() {
        self.providesPresentationContextTransitionStyle = true
        self.modalPresentationStyle = .overCurrentContext
        self.modalTransitionStyle = .crossDissolve
    }
    
}
