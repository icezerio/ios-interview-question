//
//  SummaryObject.swift
//  Question
//
//  Created by Icezerio on 6/1/2564 BE.
//

import SwiftyJSON
import Alamofire


struct SummaryObject {
    let image: String
    let title: String
    let discount: Int
    let price: Int
    let status: Bool
    let time : String
}

extension SummaryObject {
    
    static func parseJSON(_ json: JSON) -> SummaryObject {
        let image = json["image"].stringValue
        let title = json["title"].stringValue
        let discount = json["discount"].intValue
        let price = json["price"].intValue
        let status = json["status"].boolValue
        let time = json["time"].stringValue
    
        return SummaryObject(image: image, title: title, discount: discount, price: price, status: status, time: time)
    }
    
    static func parseJSONArray(_ json: JSON) -> [SummaryObject] {
        return json.map { (_, js) in SummaryObject.parseJSON(js) }
    }
    
}
