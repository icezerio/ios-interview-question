//
//  MenuObject.swift
//  Question
//
//  Created by Icezerio on 6/1/2564 BE.
//

import SwiftyJSON
import Alamofire


struct MenuObject {
    let image: String
    let name: String
}

extension MenuObject {
    
    static func parseJSON(_ json: JSON) -> MenuObject {
        let image = json["image"].stringValue
        let name = json["name"].stringValue
    
        return MenuObject(image: image, name: name)
    }
    
    static func parseJSONArray(_ json: JSON) -> [MenuObject] {
        return json.map { (_, js) in MenuObject.parseJSON(js) }
    }
    
}
