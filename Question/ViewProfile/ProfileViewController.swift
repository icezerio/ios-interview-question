//
//  ProfileViewController.swift
//  Question
//
//  Created by Icezerio on 5/1/2564 BE.
//

import UIKit
import SocketIO

class ProfileViewController: BaseViewController {
    
    @IBOutlet weak var tabItem: UITabBarItem!
    var badge = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SocketHelper.shared.socket.on("new-notification") { (response, emitter) in
            self.badge += 1
            self.tabItem.badgeValue = "\(self.badge)"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }

}
